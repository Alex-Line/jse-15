package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.repository.SessionRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public Session getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Session session = new SessionRepository(entityManager).findOne(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    @NotNull
    @Override
    public Collection<Session> getAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Session> sessions = new SessionRepository(entityManager).findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessions;
    }

    @Override
    public void persist(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).persist(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@NotNull final Collection<Session> collection) {
        if (collection.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).persist(collection);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).remove(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new SessionRepository(entityManager).merge(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Session session = new SessionRepository(entityManager).findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

}