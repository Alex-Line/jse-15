package com.iteco.linealex.jse.endpoint;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.jse.api.endpoint.IUserEndpoint;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.dto.Domain;
import com.iteco.linealex.jse.dto.SessionDto;
import com.iteco.linealex.jse.dto.UserDto;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.system.ThereIsNotSuchFileException;
import com.iteco.linealex.jse.exception.user.LowAccessLevelException;
import com.iteco.linealex.jse.exception.user.UserIsNotExistException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = getBootstrap().getUserService().getEntityById(entityId);
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return User.toUserDto(user);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<UserDto> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return User.toUsersDto(new ArrayList<>(getBootstrap().getUserService().getAllEntities()));
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception {
        if (entity == null) throw new Exception("Null user!");
        getBootstrap().getUserService().persist(UserDto.toUser(getBootstrap(), entity));
    }

    @Override
    @WebMethod
    public void persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<UserDto> collection
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        for (@Nullable final UserDto user : collection) {
            if(user == null) continue;
            persistUser(user);
        }
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        getBootstrap().getUserService().removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        getBootstrap().getUserService().removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        getBootstrap().getUserService().removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return User.toUserDto(getBootstrap().getUserService().logInUser(login, password));
    }

    @Override
    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws Exception {
        validateSession(session);
        getBootstrap().getSessionService().removeEntity(session.getId());
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        getBootstrap().getUserService().createUser(UserDto.toUser(getBootstrap(), user),
                UserDto.toUser(getBootstrap(), selectedUser));
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception {
        validateSession(session);
        if (entity == null) throw new UserIsNotExistException();
        if (session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        getBootstrap().getUserService().merge(UserDto.toUser(getBootstrap(), entity));
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        if (selectedUser == null) throw new UserIsNotExistException();
        if (!selectedUser.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        getBootstrap().getUserService().updateUserPassword(oldPassword, newPassword,
                UserDto.toUser(getBootstrap(), selectedUser));
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = getBootstrap().getUserService().getUser(login,
                UserDto.toUser(getBootstrap(), selectedUser));
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return User.toUserDto(user);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<UserDto> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        return User.toUsersDto(new ArrayList<>(getBootstrap()
                .getUserService().getAllUsers(UserDto.toUser(getBootstrap(), selectedUser))));
    }

    @Override
    @WebMethod
    public void loadBinary(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(getBootstrap()
                .getPropertyService().getProperty("SAVE_PATH") + "data.bin");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final FileInputStream inputStream = new FileInputStream(savedFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain.getUsers() == null) getBootstrap().getUserService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) getBootstrap().getProjectService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) getBootstrap().getTaskService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getTaskService().persist(domain.getTasks());
        if (domain.getSessions() == null) getBootstrap().getSessionService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadFasterJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(getBootstrap()
                .getPropertyService().getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) getBootstrap().getUserService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) getBootstrap().getProjectService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) getBootstrap().getTaskService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getTaskService().persist(domain.getTasks());
        if (domain.getSessions() == null) getBootstrap().getSessionService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadFasterXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(getBootstrap().getPropertyService().getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) getBootstrap().getUserService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) getBootstrap().getProjectService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) getBootstrap().getTaskService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getTaskService().persist(domain.getTasks());
        if (domain.getSessions() == null) getBootstrap().getSessionService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadJaxbJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(getBootstrap()
                .getPropertyService().getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        System.out.println(System.getProperty("java.classpath"));
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) getBootstrap().getUserService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) getBootstrap().getProjectService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) getBootstrap().getTaskService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getTaskService().persist(domain.getTasks());
        if (domain.getSessions() == null) getBootstrap().getSessionService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadJaxbXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(getBootstrap()
                .getPropertyService().getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) getBootstrap().getUserService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) getBootstrap().getProjectService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) getBootstrap().getTaskService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getTaskService().persist(domain.getTasks());
        if (domain.getSessions() == null) getBootstrap().getSessionService().persist(Collections.EMPTY_LIST);
        else getBootstrap().getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void saveBinary(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(getBootstrap().getUserService(), getBootstrap().getProjectService(),
                getBootstrap().getTaskService(), getBootstrap().getSessionService());
        @Nullable final String savePath = getBootstrap().getPropertyService().getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.bin");
        @NotNull final FileOutputStream outputStream = new FileOutputStream(saveFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    @Override
    @WebMethod
    public void saveFasterJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(getBootstrap().getUserService(), getBootstrap().getProjectService(),
                getBootstrap().getTaskService(), getBootstrap().getSessionService());
        @Nullable final String savePath = getBootstrap().getPropertyService().getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveFasterXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(getBootstrap().getUserService(), getBootstrap().getProjectService(),
                getBootstrap().getTaskService(), getBootstrap().getSessionService());
        @Nullable final String savePath = getBootstrap().getPropertyService().getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveJaxbJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(getBootstrap().getUserService(), getBootstrap().getProjectService(),
                getBootstrap().getTaskService(), getBootstrap().getSessionService());
        @Nullable final String savePath = getBootstrap().getPropertyService().getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    @WebMethod
    public void saveJaxbXml(
            @Nullable SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(getBootstrap().getUserService(), getBootstrap().getProjectService(),
                getBootstrap().getTaskService(), getBootstrap().getSessionService());
        @Nullable final String savePath = getBootstrap().getPropertyService().getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

}