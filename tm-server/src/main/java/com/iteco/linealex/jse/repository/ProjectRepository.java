package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public final class ProjectRepository extends AbstractTMRepository<Project> implements IRepository<Project> {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean contains(@NotNull final String entityId) {
        @Nullable final Project project = findOne(entityId);
        if (project != null) return true;
        return false;
    }

    @Override
    public boolean contains(
            @NotNull final String name,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final Project project = entityManager.createQuery("SELECT p FROM Project p " +
                "WHERE p.user.id= :userId AND p.name= :name", Project.class).setParameter("userId", userId)
                .setParameter("name", name).getSingleResult();
        if (project != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        return entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.id = :projectId", Project.class)
                .setParameter("projectId", projectId).getResultList();
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) {
        try {
            return entityManager.find(Project.class, id);
        } catch (Exception e){
            return null;
        }
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "AND (p.name= :pattern OR p.description= :pattern)", Project.class).setParameter("userId", userId)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(@NotNull final String pattern) {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "WHERE (p.name= :pattern OR p.description= :pattern)", Project.class)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.dateStart", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.dateStart", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.dateFinish", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.dateFinish", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.status", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.status", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String entityName) {
        try {
            return entityManager.createQuery("SELECT p FROM Project p WHERE p.name= : entityName", Project.class)
                    .setParameter("entityName", entityName).getSingleResult();
        } catch (Exception e){
            return null;
        }
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) {
        try {
            return entityManager.createQuery("SELECT p FROM Project p " +
                    "WHERE p.name= : entityName AND p.user.id= : userId", Project.class)
                    .setParameter("entityName", entityName).setParameter("userId", userId)
                    .getSingleResult();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public void persist(@NotNull final Project example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<Project> collection) {
        for (Project example : collection) {
            entityManager.persist(example);
        }
    }

    @Override
    public void merge(@NotNull final Project example) {
        entityManager.merge(example);
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project p").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        Collection<Project> collection = findAll(userId);
        for (@Nullable final Project project : collection) {
            if(project == null) continue;
            if (project.getUser().getId().equals(userId)) entityManager.remove(project);
        }
    }

}