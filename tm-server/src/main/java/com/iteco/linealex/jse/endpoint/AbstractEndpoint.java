package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.dto.SessionDto;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.exception.session.SessionExpiredException;
import com.iteco.linealex.jse.exception.session.ThereIsNotSuchSessionException;
import com.iteco.linealex.jse.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

@Getter
@Setter
@WebService
@NoArgsConstructor
public class AbstractEndpoint {

    @NotNull
    private Bootstrap bootstrap;

    public AbstractEndpoint(
            @NotNull final Bootstrap bootstrap
    ) {
        this.bootstrap = bootstrap;
    }

    @WebMethod
    public void validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto userSession
    ) throws Exception {
        if (userSession == null) throw new ThereIsNotSuchSessionException();
        @Nullable final Session session = bootstrap.getSessionService()
                .getSession(userSession.getUserId(), userSession.getId());
        if (session == null) throw new ThereIsNotSuchSessionException();
        if (session.getSignature() == null) throw new ThereIsNotSuchSessionException();
        if (!session.getSignature().equals(ApplicationUtils.getSignature(userSession, bootstrap.getPropertyService())))
            throw new ThereIsNotSuchSessionException();
        if (new Date().getTime() - session.getCreationDate().getTime() >
                Long.parseLong(bootstrap.getPropertyService()
                        .getProperty("SESSION_LIFE_TIME"))) throw new SessionExpiredException();
    }

}