package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    public TaskService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAll(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Task task = new TaskRepository(entityManager).findOne(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntities() throws SQLException {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).persist(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<Task> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Task task : collection) {
            if (task == null) continue;
            persist(task);
        }
    }

    @Override
    public void merge(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).merge(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Task getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Task task = new TaskRepository(entityManager).findOneByName(entityName);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Task task = new TaskRepository(entityManager).findOneByName(userId, entityName);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAllByName(userId, pattern);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAllByName(pattern);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate() throws Exception {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAllSortedByStartDate();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByStartDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAllSortedByFinishDate();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByFinishDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() throws Exception {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks = new TaskRepository(entityManager).findAllSortedByStatus();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByStatus(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Override
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).remove(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws SQLException {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new TaskRepository(entityManager).removeAll(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Task task =
                new TaskRepository(entityManager).findOneByName(userId, projectId, taskName);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllByName(userId, projectId, pattern);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByStartDate(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByFinishDate(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<Task> tasks =
                new TaskRepository(entityManager).findAllSortedByStatus(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

}