package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.entity.Project;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectRepository {

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects;")
    Collection<Project> findAll() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId};")
    Collection<Project> findAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId} AND projectId = #{projectId};")
    Collection<Project> findAllByUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE projectId = #{projectId};")
    Project findOneByProjectId(
            @NotNull @Param("projectId") final String entityId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId}" +
            "AND (projectName LIKE '%#{pattern}%' OR projectDescription LIKE '%#{pattern}%' );")
    Collection<Project> findAllByNameAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("pattern") final String pattern
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "AND (projectName LIKE '%#{pattern}%' OR projectDescription LIKE '%#{pattern}%');")
    Collection<Project> findAllByName(
            @NotNull @Param("pattern") final String pattern
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId} " +
            "ORDER BY projectStartDate;")
    Collection<Project> findAllSortedByStartDateAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "ORDER BY projectStartDate;")
    Collection<Project> findAllSortedByStartDate() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId} " +
            "ORDER BY projectFinishDate;")
    Collection<Project> findAllSortedByFinishDateAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "ORDER BY projectFinishDate;")
    Collection<Project> findAllSortedByFinishDate() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId} " +
            "ORDER BY FIELD(projectStatus, 'PLANNED', 'PROCESSING', 'DONE');")
    Collection<Project> findAllSortedByStatusAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "ORDER BY FIELD(projectStatus, 'PLANNED', 'PROCESSING', 'DONE');")
    Collection<Project> findAllSortedByStatus() throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE projectName = #{projectName};")
    Project findOneByName(
            @NotNull @Param("projectName") final String entityName
    ) throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "projectId", id = true),
            @Result(property = "name", column = "projectName"),
            @Result(property = "description", column = "projectDescription"),
            @Result(property = "dateStart", column = "projectStartDate"),
            @Result(property = "dateFinish", column = "projectFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "status", column = "projectStatus")})
    @Select("SELECT * FROM TaskManager.projects " +
            "WHERE users_userId = #{userId} AND projectName = #{projectName};")
    Project findOneByNameAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectName") final String entityName
    ) throws Exception;

    @Insert("INSERT INTO TaskManager.projects " +
            "VALUES (#{id}, #{name}, #{description}, #{status}, #{dateStart}, #{dateFinish}, #{userId})")
    void persist(
            @NotNull final Project example
    ) throws Exception;

    @Update("UPDATE TaskManager.projects " +
            "SET projectName = #{name}, projectDescription = #{description}, projectStatus = #{status}, " +
            "projectStartDate = #{dateStart}, projectFinishDate = #{dateFinish}, users_userId = #{userId} " +
            "WHERE projectId = #{id}")
    void merge(
            @NotNull final Project example
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.projects WHERE projectId = #{projectId};")
    void remove(
            @NotNull @Param("projectId") final String entityId
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.projects;")
    void removeAll() throws Exception;

    @Delete("DELETE FROM TaskManager.projects WHERE users_userId = #{userId};")
    void removeAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

}