package com.iteco.linealex.jse.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.dto.SessionDto;
import com.iteco.linealex.jse.exception.entity.WrongDateFormatException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ApplicationUtils {

    /**
     * Utils for work with dates
     */
    @NotNull
    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static String formatDateToString(@Nullable final Date date) {
        if (date == null) return FORMAT.format(new Date());
        return FORMAT.format(date);
    }

    public static Date formatStringToDate(@Nullable final String stringDate) throws WrongDateFormatException {
        try {
            return FORMAT.parse(stringDate);
        } catch (ParseException e) {
            throw new WrongDateFormatException();
        }
    }

    /**
     * Utils for work with signatures
     */
    @Nullable
    public static String getSignature(
            @NotNull final SessionDto userSession,
            @NotNull final IPropertyService propertyService
    ) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        try {
            @NotNull final String json = mapper.writeValueAsString(userSession);
            @NotNull String result = TransformatorToHashMD5.getHash(json,
                    propertyService.getProperty("SESSION_SALT"),
                    Integer.parseInt(propertyService.getProperty("SESSION_TIMES")));
            return result;
        } catch (JsonProcessingException | NoSuchAlgorithmException e) {
            return null;
        }
    }

}