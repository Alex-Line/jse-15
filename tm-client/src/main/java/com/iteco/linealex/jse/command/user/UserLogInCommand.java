package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.SessionDto;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.api.endpoint.UserDto;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "SIGN IN INTO YOUR ACCOUNT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @Nullable final SessionDto session = serviceLocator.getSessionEndpoint().createSession(login, password);
        @Nullable final UserDto user = serviceLocator.getUserEndpoint().logInUser(login, password);
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        serviceLocator.setSession(session);
        System.out.println("[USER " + user.getLogin() + " ENTERED TO TASK MANAGER]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}