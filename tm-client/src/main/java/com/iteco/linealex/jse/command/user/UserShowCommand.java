package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR ADMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        if (session == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        @Nullable final UserDto selectedUser = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        if (selectedUser == null) throw new TaskManagerException_Exception();
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (session.getRole() == Role.ADMINISTRATOR) {
            @Nullable final UserDto user = serviceLocator.getUserEndpoint().getUserByLogin(session, login, selectedUser);
            printUser(user);
            System.out.println();
            return;
        }
        if (login.equals(selectedUser.getLogin())) {
            printUser(selectedUser);
            System.out.println();
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}