package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        if (session.getRole() != Role.ADMINISTRATOR) {
            projectEndpoint.removeAllProjectsByUserId(session, session.getUserId());
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        } else {
            projectEndpoint.removeAllProjects(session);
            System.out.println("[All PROJECTS REMOVED]\n");
        }

        System.out.println("[THERE IS NOT ANY PROJECTS FOR REMOVING]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}