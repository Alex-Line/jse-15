package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        System.out.println("REMOVING TASK...");
        @Nullable TaskDto task = null;
        if (selectedProject != null) {
            task = serviceLocator.getTaskEndpoint()
                    .getTaskByNameWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId(), taskName);
        } else task = serviceLocator.getTaskEndpoint()
                .getTaskByNameWithUserId(session, session.getUserId(), taskName);
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        serviceLocator.getTaskEndpoint().removeTask(session, task.getId());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}