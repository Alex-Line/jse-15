package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.Collection;
import java.util.Collections;

public final class ProjectListByStartDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list-start";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST PROJECTS BY START DATE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<ProjectDto> collection = Collections.EMPTY_LIST;
        if (session.getRole() == Role.ADMINISTRATOR) {
            collection = serviceLocator.getProjectEndpoint().getAllProjectsSortedByStartDate(session);
        } else collection = serviceLocator.getProjectEndpoint()
                .getAllProjectsSortedByStartDateWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final ProjectDto project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}