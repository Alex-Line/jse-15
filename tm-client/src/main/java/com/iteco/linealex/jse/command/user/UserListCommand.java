package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.Collection;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL USERS REGISTERED IN THE TASK MANAGER. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        @Nullable final UserDto selectedUser = serviceLocator.getUserEndpoint()
                .getUserById(session, session.getUserId());
        if (selectedUser == null) throw new TaskManagerException_Exception();
        if (session.getRole() != Role.ADMINISTRATOR)
            throw new TaskManagerException_Exception();
        @NotNull final Collection<UserDto> collection = serviceLocator.getUserEndpoint().getAllUsers(session);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY USERS YET]\n");
            return;
        }
        int index = 1;
        for (@NotNull final UserDto user : collection) {
            System.out.print(index + ". ");
            printUser(user);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}