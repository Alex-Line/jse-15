package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.Collection;
import java.util.Collections;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL TASKS IN THE SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        @NotNull Collection<TaskDto> collection = Collections.EMPTY_LIST;
        if (selectedProject != null) {
            collection = serviceLocator.getTaskEndpoint()
                    .getAllTasksWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId());
        } else collection = serviceLocator.getTaskEndpoint().getAllTasksWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
            return;
        }
        System.out.println("[TASK LIST]");
        int index = 1;
        for (@NotNull final TaskDto task : collection) {
            System.out.println(index + ". " + "Task " + task.getName() + " {" +
                    "ID = " + task.getId() +
                    ", \n    description='" + task.getDescription() + '\'' +
                    ", \n    Start date = " + task.getDateStart() +
                    ", \n    Finish date = " + task.getDateFinish() +
                    ", \n    Status = " + task.getStatus().name() +
                    ", \n    PROJECT_ID = " + task.getProjectId() +
                    ", \n    User ID = " + task.getUserId() +
                    '}');
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}