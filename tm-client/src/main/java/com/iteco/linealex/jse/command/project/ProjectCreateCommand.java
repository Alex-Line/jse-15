package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW PROJECT AND SET SELECTION ON IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final ProjectDto project = new ProjectDto();
        project.setUserId(session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        project.setName(projectName);
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        @NotNull final String projectDescription = serviceLocator.getTerminalService().nextLine();
        project.setDescription(projectDescription);
        System.out.println("[ENTER PROJECT START DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateStart(ApplicationUtils.toXMLGregorianCalendar(ApplicationUtils.formatStringToDate(
                serviceLocator.getTerminalService().nextLine())));
        System.out.println("[ENTER PROJECT FINISH DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(ApplicationUtils.formatStringToDate(
                serviceLocator.getTerminalService().nextLine())));
        serviceLocator.getProjectEndpoint().persistProject(session, project);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}