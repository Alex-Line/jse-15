
package com.iteco.linealex.jse.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.iteco.linealex.jse.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetProperty_QNAME = new QName("http://endpoint.api.jse.linealex.iteco.com/", "getProperty");
    private final static QName _GetPropertyResponse_QNAME = new QName("http://endpoint.api.jse.linealex.iteco.com/", "getPropertyResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.iteco.linealex.jse.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetProperty }
     * 
     */
    public GetProperty createGetProperty() {
        return new GetProperty();
    }

    /**
     * Create an instance of {@link GetPropertyResponse }
     * 
     */
    public GetPropertyResponse createGetPropertyResponse() {
        return new GetPropertyResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProperty }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProperty }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.jse.linealex.iteco.com/", name = "getProperty")
    public JAXBElement<GetProperty> createGetProperty(GetProperty value) {
        return new JAXBElement<GetProperty>(_GetProperty_QNAME, GetProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPropertyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPropertyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.jse.linealex.iteco.com/", name = "getPropertyResponse")
    public JAXBElement<GetPropertyResponse> createGetPropertyResponse(GetPropertyResponse value) {
        return new JAXBElement<GetPropertyResponse>(_GetPropertyResponse_QNAME, GetPropertyResponse.class, null, value);
    }

}
