package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING THE USER BY LOGIN";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final UserDto selectedUser = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                serviceLocator.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(serviceLocator.getPropertyEndpoint().getProperty("PASSWORD_TIMES")));
        if (hashPassword.equals(selectedUser.getHashPassword())) {
            @Nullable final UserDto user = serviceLocator.getUserEndpoint().getUserByLogin(session, login, selectedUser);
            if (user == null) throw new TaskManagerException_Exception();
            serviceLocator.getUserEndpoint().removeUserById(session, user.getId());
            serviceLocator.getProjectEndpoint().removeAllProjectsByUserId(session, user.getId());
            serviceLocator.getTaskEndpoint().removeAllTasksWithUserId(session, user.getId());
            System.out.println("[USER " + selectedUser.getLogin() + " REMOVED]");
            System.out.println("[ALSO WAS REMOVED ALL IT'S PROJECTS AND TASKS]");
            System.out.println("[OK]\n");
        } else throw new TaskManagerException_Exception();
    }

    @Override
    public boolean secure() {
        return true;
    }

}