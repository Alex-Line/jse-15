package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersistUserIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void persistUsersPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        @NotNull final UserDto newUser2 = new UserDto();
        newUser2.setLogin("test2");
        newUser2.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser2.setRole(Role.ORDINARY_USER);
        newUser2.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        bootstrap.getUserEndpoint().persistUser(newUser2);
        @NotNull final List<UserDto> resultList = bootstrap.getUserEndpoint().getAllUsers(adminSession);
        resultList.sort(Comparator.comparing(UserDto::getLogin));
        assertEquals(resultList.get(1).getId(), newUser.getId());
        assertEquals(resultList.get(2).getId(), newUser2.getId());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser2.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        @Nullable final Throwable thrown1 = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser2.getId());
        });
        assertNotNull(thrown1.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persistUsersNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().persistUser(null);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}