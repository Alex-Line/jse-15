package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetUserByLoginIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void getUserByLoginNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserByLogin(null, newUser.getLogin(), admin);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void getUserByLoginNegative2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserByLogin(userSession, newUser.getLogin(), user);
        });
        assertNotNull(thrown.getMessage());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}